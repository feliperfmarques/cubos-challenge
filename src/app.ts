import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import { RulesRoutes } from "./routes/rules-routes";
import { SchedulesRoutes } from "./routes/schedules-routes";
import * as dotenv from "dotenv";

class App {

    public app: express.Application;
    private rulesRoutes: RulesRoutes = new RulesRoutes();
    private schedulesRoutes: SchedulesRoutes = new SchedulesRoutes();

    constructor() {
        this.app = express();
        this.config();
        dotenv.config();
        let opts = {
            user: `${process.env.MONGO_ROOT_USER}`,
            pass: `${process.env.MONGO_ROOT_PASSWORD}`,
            auth: {
                authdb: `${process.env.MONGO_AUTH_DATABASE}`
            },
            useNewUrlParser: true
        };
        mongoose.connect(`mongodb://${process.env.MONGO_ADDRESS}:
          ${process.env.MONGO_PORT}/${process.env.MONGO_AUTH_DATABASE}`, opts);
        this.rulesRoutes.routes(this.app);
        this.schedulesRoutes.routes(this.app);
    }

    private config(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        // support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

}

export default new App().app;
