import { Request, Response } from "express";
import { SchedulesController } from "../controllers/schedules-controller";

export class SchedulesRoutes {

    private schedulesController: SchedulesController = new SchedulesController();

    public routes(app): void {

        app.route("/clinic/schedule")
        .get(this.schedulesController.getSchedules);
    }
}
