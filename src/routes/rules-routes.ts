import { Request, Response } from "express";
import { RulesController } from "../controllers/rules-controller";

export class RulesRoutes {

    private rulesController: RulesController = new RulesController();

    public routes(app): void {

        app.route("/clinic/rules")
        .get(this.rulesController.getAllRules)
        .post(this.rulesController.addNewRule)
        .put(this.rulesController.updateRule)
        .delete(this.rulesController.removeRule);
    }
}
