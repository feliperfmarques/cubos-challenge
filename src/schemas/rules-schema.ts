import * as mongoose from "mongoose";
import * as moment from "moment";

const Schema = mongoose.Schema;

export const RulesSchema = new Schema({
  type: {
    type: String,
    enum: ["specific", "daily", "weekly", "fortnightly", "monthly"],
    required: "Enter with rule 'type'",
  },
  day: {
    type: Number,
    validate: {
      validator: function(day) {
        return day >= 1 && day <= 31;
      },
      message: (props) => {
        return props.value.length > 0 ? `${props.value} is not a valid 'day'!` :
        "Enter with a valid 'day'";
      }
    }
  },
  weeks: {
    type: [Number],
    required: (this.type == "fortnightly") ? true : false,
    default: void 0,
    validate: {
      validator: function(weeks) {
        return this.type != "fortnightly" || weeks.length == 2 &&
        (weeks[0] >= 1 && weeks[0] <= 5) && (weeks[1] >= 1 && weeks[1] <= 5) &&
        (weeks[1] - weeks[0]) == 2;
      },
      message: (props) => {
        return props.value.length > 0 ? `${props.value} is not a valid Array of weeks!` :
        "Enter with a valid Array of 'weeks'";
      }
    }
  },
  date: {
    type: Date,
    set: function(date) {
      if (moment(date, "DD-MM-YYYY", true).isValid()) {
        return moment(date, "DD-MM-YYYY").startOf("day").toDate();
      } else {
        return new Error("Enter with a valid 'date'");
      }
    },
    get: function(date) {
      if (date) {
        return moment(date).startOf("day").format("DD-MM-YYYY");
      } else {
        return undefined;
      }
    }
  },
  interval: {
    type: [{
      _id: false,
      weekDay: {
        type: Number,
        required: (this.type == "weekly") || (this.type == "fortnightly") ?
        true : false
      },
      start: {
        type: String,
        required: true,
      },
      end: {
        type: String,
        required: true
      }
    }],
    validate: {
      validator: function(intervals) {
        return intervals.length > 0 && intervals.every((interval, index) => {
          return moment(interval.start, "HH:mm", true).isValid() &&
          moment(interval.end, "HH:mm", true).isValid() &&
          ((this.type == "weekly") || (this.type == "fortnightly") ?
            (interval.weekDay >= 1 && interval.weekDay <= 7) &&
            intervals.every((_interval, _index) => {
              return (index != _index) ? (interval.weekDay != _interval.weekDay) : true;
            })
            : true
          );
        });
      },
      message: (props) => {
        return props.value.length > 0 ? `${props.value} is not a valid interval!` :
        "Enter with a valid 'interval'";
      }
    },
  }
})
.pre("save", function(next) {

  let query: object = {
    "type": this.type
  };

  if (this.type == "specific") {
    query["date"] = this.date;
  }

  if (this.type == "fortnightly") {
    query["weeks"] = {
      "$in": this.weeks
    };
  }

  if (this.type == "monthly") {
    query["day"] = this.day;
  }

  this.constructor.find(query, (error, data) => {
    if (data.length > 0) {
      next(new Error("Rule already registered! Try update it."));
    } else {
      next();
    }
  });
});
