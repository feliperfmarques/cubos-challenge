import * as m from "moment";
import * as mongoose from "mongoose";
import { RulesSchema } from "../schemas/rules-schema";
import { Request, Response } from "express";
import { extendMoment } from "moment-range";

const moment = extendMoment(m);
const Rules = mongoose.model("RulesSchema", RulesSchema);

export class SchedulesController {

  public getSchedules (req: Request, res: Response) {

    try {

      let start;
      let end;

      if (
        moment(req.query.start, "DD-MM-YYYY", true).isValid() &&
        moment(req.query.end, "DD-MM-YYYY", true).isValid()
      ) {
        start = moment(req.query.start, "DD-MM-YYYY").startOf("day");
        end = moment(req.query.end, "DD-MM-YYYY").add(1, "day").startOf("day");
      } else {
        res.status(400).json({"errors": "Enter with a valid dates for 'start' and 'end'"});
        return;
      }

      let schedules: Array<object> = [];

      function weekOfMonth(m) {
        return m.week() - moment(m).startOf("month").week() + 1;
      }

      function findRules( d, callback ) {
        if (d.isBefore(end)) {
          Rules.find({
            $or: [
              {
                "type": "specific",
                "date": d.toDate()
              },
              {
                "type": "daily"
              },
              {
                "type": "weekly",
                "interval": {
                  $elemMatch: {
                    "weekDay": d.isoWeekday()
                  }
                }
              },
              {
                "type": "fortnightly",
                "weeks": {
                  "$in": [weekOfMonth(d)]
                },
                "interval": {
                  $elemMatch: {
                    "weekDay": d.isoWeekday()
                  }
                }
              },
              {
                "type": "monthly",
                "day": d.date()
              }
            ]
          },
          (err, docs) => {
            if (err) {
              res.status(400).json({"error": "Bad request"});
              return;
            } else {
              let intervals = [];
              docs.forEach((doc) => {
                doc.interval.forEach((interval) => {
                  if (((doc.type == "fortnightly" || doc.type == "weekly") &&
                    interval.weekDay == d.isoWeekday()) || doc.type == "specific" ||
                    doc.type == "daily" || doc.type == "monthly"
                  ) {
                    intervals.push({"start": interval.start, "end": interval.end});
                  }
                });
              });
              if (intervals.length > 0) {
                schedules.push({"day": d.format("DD-MM-YYYY"), "interval": intervals});
              }
              findRules(d.add(1, "days"), callback);
            }
          });
        } else {
          callback();
        }
      }

      findRules(start, () => {
        res.status(200).json(schedules);
        return;
      });

    } catch (err) {
      res.status(400).json({"error": "Bad request"});
    }
  }
}
