import * as mongoose from "mongoose";
import { RulesSchema } from "../schemas/rules-schema";
import { Request, Response } from "express";
import * as moment from "moment";

const Rules = mongoose.model("RulesSchema", RulesSchema);

export class RulesController {

  public getAllRules(req: Request, res: Response) {
    Rules.find({}, undefined, {sort: "date"}, (err, docs) => {
      if (err) {
        try {
          let errors: Array<object> = [];
          Object.keys(err.errors).forEach((key) => {
            errors.push({[key]: err.errors[key].message});
          });
          res.status(400).json({"errors": errors});
        } catch (e) {
          res.status(400).json({"errors": err.message});
        }
      } else {
        docs = docs.map((doc) => {
          if (doc.date) {
            return {
              type: doc.type,
              date: doc.date,
              interval: doc.interval
            };
          } else if (doc.day) {
            return {
              type: doc.type,
              day: doc.day,
              interval: doc.interval
            };
          } else if (doc.weeks) {
            return {
              type: doc.type,
              weeks: doc.weeks,
              interval: doc.interval
            };
          }
          else {
            return {
              type: doc.type,
              interval: doc.interval
            };
          }
        });
        res.status(200).json(docs);
      }
    });
  }

  public addNewRule(req: Request, res: Response) {

    if (
      !(req.body.type == "specific") && !(req.body.type == "daily") &&
      !(req.body.type == "weekly") && !(req.body.type == "fortnightly") &&
      !(req.body.type == "monthly")
    ) {
      res.status(400).json({"errors": "Enter with a valid 'type'"});
      return;
    }

    if (req.body.type == "specific" && !req.body.date) {
      res.status(400).json({"errors": "Enter with a field 'date'"});
      return;
    }

    if (req.body.type == "monthly" && !req.body.day) {
      res.status(400).json({"errors": "Enter with a field 'day'"});
      return;
    }

    if (req.body.type == "fortnightly" && !req.body.weeks) {
      res.status(400).json({"errors": "Enter with a field 'weeks'"});
      return;
    }

    const newRule = new Rules(req.body);

    newRule.save((err, rule) => {
        if (err) {
          try {
            let errors: Array<object> = [];
            Object.keys(err.errors).forEach((key) => {
              errors.push({[key]: err.errors[key].message});
            });
            res.status(400).json({"errors": errors});
          } catch (e) {
            res.status(400).json({"errors": err.message});
          }
        } else {
          res.status(200).json(rule);
        }
    });
  }

  public updateRule(req: Request, res: Response) {

    if (
      !(req.body.type == "specific") && !(req.body.type == "daily") &&
      !(req.body.type == "weekly") && !(req.body.type == "fortnightly") &&
      !(req.body.type == "monthly")
    ) {
      res.status(400).json({"errors": "Enter with a valid 'type'"});
      return;
    }

    if (req.body.type == "specific" && !req.body.date) {
      res.status(400).json({"errors": "Enter with a field 'date'"});
      return;
    }

    if (req.body.type == "monthly" && !req.body.day) {
      res.status(400).json({"errors": "Enter with a field 'day'"});
      return;
    }

    if (req.body.type == "fortnightly" && !req.body.weeks) {
      res.status(400).json({"errors": "Enter with a field 'weeks'"});
      return;
    }

    let query: any = undefined;

    if (req.body.type) {
      query = {
        "type": req.body.type
      };

      if (req.body.type == "fortnightly") {
        if (req.body.weeks) {
          query["weeks"] = {
            "$in": req.body.weeks
          };
        } else {
          res.status(400).json({"errors": "Enter with a valid 'weeks'(This field "
          + "is necessary when 'fortnightly' rule type is choose.)"});
          return;
        }
      }

      if (req.body.type == "monthly") {
        if (req.body.day) {
          query["day"] = req.body.day;
        } else {
          res.status(400).json({"errors": "Enter with a valid 'date'(This field "
          + "is necessary when 'monthly' rule type is choose.)"});
          return;
        }
      }

      if (req.body.type == "specific") {
        if (req.body.date) {
          query["date"] = req.body.date;
        } else {
          res.status(400).json({"errors": "Enter with a valid 'date'(This field "
          + "is necessary when specific rule type is choose.)"});
          return;
        }
      }

      Rules.findOneAndUpdate(
        query, req.body,
        { runValidators: true, context : "query", new: true},
        (err, rule) => {
          if (!err && ! rule) {
            res.status(404).json({"errors": "Not Found"});
          }
          else if (err) {
            try {
              let errors: Array<object> = [];
              Object.keys(err.errors).forEach((key) => {
                errors.push({[key]: err.errors[key].message});
              });
              res.status(400).json({"errors": errors});
            } catch (e) {
              res.status(400).json({"errors": err.message});
            }
          } else {
            res.status(200).json(rule);
          }
      });

    } else {
      res.status(400).json({"errors": "Enter with 'type' field."});
    }
  }

  public removeRule(req: Request, res: Response) {
    if (
      req.body.type != "daily" && req.body.type != "weekly" &&
      req.body.type != "fortnightly" && req.body.type != "monthly" &&
      req.body.type != "specific" && !(req.body.type == "specific" &&
      req.body.date)
    ) {
      res.status(400).json({"errors": "Enter with a valid 'type'('date' field is "
      + "necessary when 'specific' rule type is choose. )"});
      return;
    }

    let query: any = undefined;

    if (req.body.type) {
      query = {
        "type": req.body.type
      };

      if (req.body.type == "fortnightly") {
        if (req.body.weeks) {
          query["weeks"] = {
            "$in": req.body.weeks
          };
        } else {
          res.status(400).json({"errors": "Enter with a valid 'weeks'(This field "
          + "is necessary when 'fortnightly' rule type is choose.)"});
          return;
        }
      }

      if (req.body.type == "monthly") {
        if (req.body.day) {
          query["day"] = req.body.day;
        } else {
          res.status(400).json({"errors": "Enter with a valid 'date'(This field "
          + "is necessary when 'monthly' rule type is choose.)"});
          return;
        }
      }

      if (req.body.type == "specific") {
        if (req.body.date) {
          if (moment(req.body.date, "DD-MM-YYYY", true).isValid()) {
            query["date"] = moment(req.body.date, "DD-MM-YYYY").startOf("day").toDate();
          } else {
            res.status(400).json({"errors": "Enter with a valid 'date'"});
            return;
          }
        } else {
          res.status(400).json({"errors": "Enter with a valid 'date'(This field "
          + "is necessary when specific rule type is choose.)"});
          return;
        }
      }

      Rules.findOneAndRemove(
        query,
        req.body,
        (err, rule) => {
          if (!err && ! rule) {
            res.status(404).json({"errors": "Not Found"});
            return;
          }
          else if (err) {
            try {
              let errors: Array<object> = [];
              Object.keys(err.errors).forEach((key) => {
                errors.push({[key]: err.errors[key].message});
              });
              res.status(400).json({"errors": errors});
            } catch (e) {
              res.status(400).json({"errors": err.message});
            }
          } else {
            res.status(200).json(rule);
          }
      });

    } else {
      res.status(400).json({"errors": "Enter with 'type' field."});
    }
  }
}
