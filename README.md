# Cubos Challenge (Simple API for Clinic Schedules Management)

Este projeto é desenvolvido utilizando a versão v8.11.X do [NODEJS](https://nodejs.org/) e versão 4.0.4
do [MogoDB](https://www.mongodb.com/). Para executar ou começar a desenvolver você deve criar o ambiente, instalando:

1. [NODEJS](https://nodejs.org/en/download/)

1. [TypeScript](https://www.typescriptlang.org/) ``` npm install -g typescript ```

Para fins de desenvolvimento pode-se utilizar o [docker stack](https://docs.docker.com/engine/reference/commandline/stack/) para subir uma instância do MongoDB e do [mongo-express](https://github.com/mongo-express/mongo-express). Para isso siga os seguintes passos:

Baixe e instale o Docker:

1. [Docker](https://www.docker.com/get-started)

Após instalar devidamente o Docker, executar os seguintes comandos:

```
docker stack deploy -c stack.yaml mongo
```

Após esse passo o MongoDB estará rodando na porta 27017 e o mongo-express na porta 8081 (Senha para acessar a interface do mongo-express foi definido nas variáveis ```ME_CONFIG_BASICAUTH_USERNAME``` e ```ME_CONFIG_BASICAUTH_PASSWORD``` no arquivo ```stack.yaml```).

Depois de instalar devidamente todas as dependências de sistema e subir a instância do mongodb e mongo-express(Opcional para visualizar dados), basta executar os seguintes comandos abaixo:

```
npm install
npm run dev
```

PS: O projeto está configurado para utilizar as variáveis de ambiente da stack docker definidas no arquivo ```.env```, caso queira modificar o projeto para rodar com outra configuração de Database e Server, basta modificar este arquivo com as variáveis corretas.

# Postman Docs

https://documenter.getpostman.com/view/5463778/RzfarC56


# Sobre o Projeto

## Especificação Swagger 2.0

Foi criado um arquivo de documentação/especificação da API utilizando o padrão Swagger 2.0. O arquivo está em ```src/swagger.yaml```. Podendo ser visualizado no Swagger UI.

## Lint

Para adequar o código as melhores práticas de desenvolvimento foi configurado o [TSLint](https://palantir.github.io/tslint/) com as regras no arquivo ```tslint.json```. Para verificar os erros e warnings do TSLint é só executar o comando abaixo:

```
npm run lint
```

## Testes

Foram criados 48 casos de testes para verificar o comportamento esperado da API com algumas possíveis opções de entrada. Os testes podem ser executados utilizando o [Mocha](https://mochajs.org/). Para executar os testes basta executar o comando abaixo com o servidor da API rodando:

```
npm run test
```
