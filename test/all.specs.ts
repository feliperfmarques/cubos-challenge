import * as request from "request";
import { expect } from "chai";
import * as dotenv from "dotenv";

dotenv.config();
const urlBase = `${process.env.SERVER_PROTOCOL}://${process.env.SERVER_ADDRESS}:${process.env.SERVER_PORT}`;

describe(
  "Testes da rota de cadastro de regras /clinic/rules (POST)",
  function() {
    it(
      "Deve retornar HTTP 400 ao tentar criar regra sem parâmetro 'type'",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "date": "04-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro type == 'specific'"
      + " e sem parâmetro 'date'",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra sem parâmetro 'interval'",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "04-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro 'interval' inválido",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "04-02-2018",
            "interval": [
              {"start": "08:00"},
              {"start": "14:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro 'interval' inválido",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "04-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro type == 'specific'"
      + "e com parâmetro 'date' inválido",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "04-02-18",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro type == 'weekly'"
      + "e sem parâmetro 'weekDay' no interval",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "04-02-18",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro type == 'fortnightly'"
      + "e sem parâmetro 'weeks'",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "fortnightly",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar criar regra com parâmetro type == 'montlhy'"
      + "e sem parâmetro 'day'",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "montlhy",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (specific)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (specific)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "14-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (specific)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "15-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (daily)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "daily",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (weekly)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "weekly",
            "interval": [
              {"weekDay": 1, "start": "08:00", "end": "12:00"},
              {"weekDay": 7, "start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (fortnightly)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "fortnightly",
            "weeks": [2, 4],
            "interval": [
              {"weekDay": 1, "start": "08:00", "end": "12:00"},
              {"weekDay": 7, "start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve cadastrar uma nova regra e retornar HTTP 200 (monthly)",
      function(done) {
        request.post(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "monthly",
            "day": 13,
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );
  }
);

describe(
  "Testes da rota de retorno de regras cadastradas /clinic/rules (GET)",
  function() {
    it(
      "Deve listar todas as regras cadastradas e retornar HTTP 200",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/rules",
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );
  }
);

describe(
  "Testes da rota de retorno de regras cadastradas dentro de um intervalo /clinic/schedule (GET)",
  function() {
    it(
      "Deve retornar HTTP 400 ao tentar retornar lista de horários sem parâmetro 'start' e 'end'",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );
    it(
      "Deve retornar HTTP 400 ao tentar retornar lista de horários sem parâmetro 'start'",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
          qs: {
            "end": "15-02-2018"
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );
    it(
      "Deve retornar HTTP 400 ao tentar retornar lista de horários sem parâmetro 'end'",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
          qs: {
            "start": "01-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );
    it(
      "Deve retornar HTTP 400 ao tentar retornar lista de horários com datas "
      + "entre as datas inválidas (dia)",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
          qs: {
            "start": "32-02-2018",
            "end": "15-02-2018"
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );
    it(
      "Deve retornar HTTP 400 ao tentar retornar lista de horários com datas "
      + "entre as datas inválidas (mês)",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
          qs: {
            "start": "01-31-2018",
            "end": "15-02-2018"
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );
    it(
      "Deve retornar HTTP 400 ao tentar retornar lista de horários com datas "
      + "entre as datas inválidas (ano)",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
          qs: {
            "start": "01-31-18",
            "end": "15-02-2018"
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );
    it(
      "Deve retornar HTTP 200 com a lista de horários disponíveis entre as datas informadas",
      function(done) {
        request.get(
        {
          url : urlBase + "/clinic/schedule",
          qs: {
            "start": "01-02-2018",
            "end": "14-02-2018"
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );
  }
);

describe(
  "Testes da rota de atualização de regras cadastradas /clinic/rules (PUT)",
  function() {
    it(
      "Deve retornar HTTP 400 ao tentar atualizar regra sem passar parâmetro 'type'",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "date": "04-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar atualizar uma regra passando parâmetro "
      + "type == 'specific' e sem parâmetro 'date'",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar atualizar uma regra com parâmetro 'interval' inválido",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-2018",
            "interval": [
              {"start": "08:00"},
              {"start": "14:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar atualizar regra com parâmetro 'interval' inválido",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar atualizar regra passando parâmetro "
      + "type == 'specific' e com parâmetro 'date' inválido",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-18",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 404 ao tentar atualizar regra inexistente",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "20-02-2018",
            "interval": [
              {"start": "08:00", "end": "12:00"},
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(404);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (specific)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-2018",
            "interval": [
              {"start": "07:00", "end": "12:00"},
              {"start": "13:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (specific)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "14-02-2018",
            "interval": [
              {"start": "07:00", "end": "12:00"},
              {"start": "13:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (specific)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "15-02-2018",
            "interval": [
              {"start": "07:00", "end": "12:00"},
              {"start": "13:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (daily)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "daily",
            "interval": [
              {"start": "08:30", "end": "11:30"},
              {"start": "13:00", "end": "19:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (weekly)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "weekly",
            "interval": [
              {"weekDay": 1, "start": "10:00", "end": "12:00"},
              {"weekDay": 5, "start": "15:00", "end": "20:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (fortnightly)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "fortnightly",
            "weeks": [2, 4],
            "interval": [
              {"weekDay": 1, "start": "10:00", "end": "12:00"},
              {"weekDay": 5, "start": "15:00", "end": "20:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve atualizar uma regra e retornar HTTP 200 (monthly)",
      function(done) {
        request.put(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "monthly",
            "day": 13,
            "interval": [
              {"start": "14:00", "end": "18:00"}
            ]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );
  }
);


describe(
  "Testes da rota de remoção de regras cadastradas /clinic/rules (DELETE)",
  function() {
    it(
      "Deve retornar HTTP 400 ao tentar remover regra sem passar parâmetro 'type'",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "date": "04-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar remover uma regra passando parâmetro "
      + "type == 'specific' e sem parâmetro 'date'",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 400 ao tentar remover regra passando parâmetro "
      + "type == 'specific' e com parâmetro 'date' inválido",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-18",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(400);

          done();
        });
      }
    );

    it(
      "Deve retornar HTTP 404 ao tentar remover regra inexistente",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "20-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(404);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (specific)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "13-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (specific)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "14-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (specific)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "specific",
            "date": "15-02-2018",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (daily)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "daily",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (weekly)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "weekly",
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (fortnightly)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "fortnightly",
            "weeks": [2, 4]
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );

    it(
      "Deve remover uma regra e retornar HTTP 200 (monthly)",
      function(done) {
        request.delete(
        {
          url : urlBase + "/clinic/rules",
          headers: {
            "Content-Type": "application/json"
          },
          json: true,
          body: {
            "type": "monthly",
            "day": 13
          }
        },
        function(error, response, body) {
          let _body = {};
          try {
            _body = JSON.parse(body);
          }
          catch (e) {
            _body = {};
          }

          expect(response.statusCode).to.equal(200);

          done();
        });
      }
    );
  }
);
